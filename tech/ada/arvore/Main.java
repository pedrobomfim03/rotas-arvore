package tech.ada.arvore;

import tech.ada.arvore.controller.RotaController;

public class Main {
	public static void main(String[] args) {
		
		RotaController controller = new RotaController();
		controller.mostrarMenu();
		
		// Root ->  A
		// - IMPLEMENTE UM ADIÇÃO A PARTIR DE UMA ROTA A PARTIR DA ORIGEM - ROOT
		
		// A -> B - Correto
		// C -> B - Não pode pois o C é final
		// - IMPLEMENTE UM ADIÇÃO A PARTIR DE UMA ROTA DO NOME DA ROTA PAI - PESQUISA O NOME DA ROTA
		
		// C
		// - CRIAR FINAIS
		
		// B -> C
		// - VINCULAR AO FINAL
		
		// Calcula a Distância
		// - IMPLEMENTE UMA FUNCIONALIDADE QUE DADO UM DESTINO A PARTIR DA ORIGEM,
		//MOSTRE O CAMINHO MAIS RAPIDO.
	}

}
