package tech.ada.arvore.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import tech.ada.arvore.model.Arvore;
import tech.ada.arvore.model.Elemento;
import tech.ada.arvore.model.Rota;

public class RotaController {
	
	Arvore<Rota> rotas = new Arvore<>();
	List<Elemento<Rota>> finais = new ArrayList<>();
	Scanner sc = new Scanner(System.in);
	
	public void mostrarMenu() {
		Boolean continuar = true;
		
		do {
			
			System.out.println("Digite a opção desejada: ");
			System.out.println("1 - Adicionar rota a partir da origem");
			System.out.println("2 - Adicionar rota a partir de outra rota");
			System.out.println("3 - Adicionar rota final");
			System.out.println("4 - Vincular rota final a outra rota comum");
			System.out.println("5 - Verificar menor distância da origem a rota final escolhida");
			System.out.println("6 - Imprimir as rotas");
			System.out.println("0 - Sair");
			
			String opcao = sc.next();
			
			switch(opcao) {
				case "1" -> adicionarRotaOrigem();
				case "2" -> adicionarRotaOutraRota();
				case "3" -> adicionarRotaFinal();
				case "4" -> vincularRotaFinal();
				case "5" -> verificarMenorDistancia();
				case "6" -> imprimirArvore();
				case "0" -> {
					System.out.println("Até mais!");
					continuar = false;
				}
				default -> System.out.println("Opção inválida!");
			}
			
		}while(continuar);
	}
	
	private void verificarMenorDistancia() {
		
		Elemento<Rota> finalSelecionado = pegarFinal();
		if(finalSelecionado==null) {
			return;
		}
	
		List<List<Rota>> caminhosFinais = new ArrayList<>();
		
		percorrerArvore(
				new ArrayList<>(),
				caminhosFinais,
				rotas.pegarTodosFilhos(),
				finalSelecionado.getInfo().getNome());
		
		Double menorDistancia = Double.MAX_VALUE;
		String caminhoMenorDistancia = "";
		
		for(List<Rota> caminho : caminhosFinais) {
			
			String rotaCompleta = String.join(" -> ", 
					caminho.stream().map(rota->rota.getNome()).toList());
			Double distanciaTotal = caminho
			.stream()
			.map(rota->rota.getDistancia())
			.reduce(0.0, (subtotal, element) -> subtotal + element);
			System.out.println("Rota : "+rotaCompleta+", Distância: "+distanciaTotal);
			
			if(distanciaTotal<menorDistancia) {
				menorDistancia = distanciaTotal;
				caminhoMenorDistancia = rotaCompleta;
			}
		}
		
		System.out.println("\n\nRota com menor distância : "+caminhoMenorDistancia+", Distância: "+menorDistancia+"\n\n");
	}
	
	private Rota criarRota() {
		
		sc.nextLine();
		System.out.println("Digite o nome da rota: ");
		String nome = sc.nextLine();
		
		System.out.println("Digite a distância em metros da rota: ");
		Double distancia = sc.nextDouble();
		
		Rota rota = new Rota();
		rota.setNome(nome);
		rota.setDistancia(distancia);
		
		return rota;
	}
	
	private void adicionarRotaOrigem() {
		
		Rota rota = criarRota();
		
		rotas.inserir(rota);
	}
	
	private void adicionarRotaOutraRota() {
		
		Rota rota = criarRota();
		
		sc.nextLine();
		System.out.println("Digite o nome da rota pai a ser vinculada: ");
		String nomePai = sc.nextLine();
		
		Elemento<Rota> pai = pegarFilhoDaArvore(rotas.pegarTodosFilhos(),nomePai);
		
		if(pai==null) {
			System.out.println("Rota pai não encontrada.");
			return;
		}
		
		Boolean paiEhFinal = finais.stream().anyMatch(fim->
			fim.getInfo().getNome().equals(pai.getInfo().getNome()));
		
		if(paiEhFinal) {
			System.out.println("Rota pai é uma rota final que não pode ter vinculos.");
			return;
		}
		
		pai.addFilho(rota);
	}
	
	private void adicionarRotaFinal(){
		
		Rota rota = criarRota();
		
		Elemento<Rota> elem = new Elemento<>(rota);
		
		finais.add(elem);
	}
	
	private Elemento<Rota> pegarFinal(){
		
		System.out.println("Digite o identificador da rota final desejada: ");
		for(int i = 0;i<finais.size();i++) {
			System.out.println("Identificador: "+i+" - Nome da Rota: "+finais.get(i).getInfo().getNome());
		}
		
		Elemento<Rota> finalSelecionado = null;
		try {
			
			Integer identificador = sc.nextInt();
			finalSelecionado = finais.get(identificador);
			
		}catch(Exception ex) {
			System.out.println("Identificador incorreto.");
			return null;
		}
		
		return finalSelecionado;
	}
	
	private void vincularRotaFinal() {
		
		Elemento<Rota> finalSelecionado = pegarFinal();
		if(finalSelecionado==null) {
			return;
		}
			
		sc.nextLine();
		System.out.println("Digite o nome da rota pai a ser vinculada: ");
		String nomePai = sc.nextLine();
		
		Elemento<Rota> pai = pegarFilhoDaArvore(rotas.pegarTodosFilhos(),nomePai);
		
		if(pai==null) {
			System.out.println("Rota pai não encontrada.");
			return;
		}
		
		pai.addFilho(finalSelecionado.getInfo());
	}
	
	private void imprimirArvore() {
		System.out.println("\n\n----- ROTAS -----\n");
		imprimirFilhos(rotas.pegarTodosFilhos(),0);
		System.out.println("\n\n");
	}
	
	private void imprimirFilhos(List<Elemento<Rota>> itens,Integer identacao) {
		for(Elemento<Rota> item : itens) {
			System.out.println(
					"\t".repeat(identacao)+
					item.getInfo().getNome()+
					" ("+item.getInfo().getDistancia()+"m)");
			imprimirFilhos(item.getFilhos(),identacao+1);
		}
	}
	
	private Elemento<Rota> percorrerArvore(
			List<Rota> caminhos,
			List<List<Rota>> caminhosFinais,
			List<Elemento<Rota>> itens, 
			String fim) {
		
		for(Elemento<Rota> item : itens) {
			caminhos.add(item.getInfo());

			if(item.getFilhos().size()==0) {
				if(item.getInfo().getNome().equals(fim)) {
					caminhosFinais.add(new ArrayList<>(caminhos));
				}
			}else {
				percorrerArvore(
						caminhos,
						caminhosFinais,
						item.getFilhos(),
						fim);
			}
	
			caminhos.remove(caminhos.size()-1);
		}
		
		return null;
	}
	
	private Elemento<Rota> pegarFilhoDaArvore(List<Elemento<Rota>> itens, String nome) {
		
		for(Elemento<Rota> item : itens) {
			if(item.getInfo().getNome().equals(nome)) {
				return item;
			}else {
				Elemento<Rota> itemRetornado = pegarFilhoDaArvore(item.getFilhos(),nome);
				if(itemRetornado!=null) {
					return itemRetornado;
				}
			}
		}
		
		return null;
	}
	
	public void cls(){
        System.out.print("\033[H\033[2J");  
        System.out.flush(); 
    }
}
